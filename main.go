package main

import (
	"bytes"
	"fmt"
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"github.com/hajimehoshi/ebiten/inpututil"
	"gitlab.com/aboitier/exploria/images"
	"image"
	"image/color"
	_ "image/png"
	_ "io/ioutil"
	"log"
	"math"
	"math/rand"
)

func getImage(imageName []byte) *ebiten.Image {
	imgdata, _, err := image.Decode(bytes.NewReader(imageName))
	if err != nil {
		log.Fatal(err)
	}
	rImage, err := ebiten.NewImageFromImage(imgdata, 1)
	return rImage
}

func initVImage(toInit *vImg, imageName []byte) {
	toInit.file = imageName
	toInit.img = getImage(imageName)
	toInit.Size.X, toInit.Size.Y = toInit.img.Size()
	toInit.center.X = toInit.Size.X / 2
	toInit.center.Y = toInit.Size.Y / 2
}

func Colision(img1, img2 vImg, distX, distY float64) bool {
	img1X := img1.op.GeoM.Element(0, 2) + float64(img1.center.X)
	img2X := img2.op.GeoM.Element(0, 2) + float64(img2.center.X)
	if math.Abs(img1X-img2X) > distX {
		return false
	}
	img1Y := img1.op.GeoM.Element(1, 2) + float64(img1.center.Y)
	img2Y := img2.op.GeoM.Element(1, 2) + float64(img2.center.Y)
	if math.Abs(img1Y-img2Y) > distY {
		return false
	}
	return true
}

func LightColision(img1 vImg, posXY image.Point, distX, distY float64) bool {
	img1X := img1.op.GeoM.Element(0, 2) + float64(img1.center.X)
	img2X := float64(posXY.X + 10)
	if math.Abs(img1X-img2X) > distX {
		return false
	}
	img1Y := img1.op.GeoM.Element(1, 2) + float64(img1.center.Y)
	img2Y := float64(posXY.Y + 10)
	if math.Abs(img1Y-img2Y) > distY {
		return false
	}
	return true
}

// Game implements ebiten.Game interface.
type Game struct {
	currentLevel     image.Point
	currentCharacter *vImg
	//	mode             int
	worldMap [BORDERS][BORDERS]level
}

type level struct {
	//	img    []*ebiten.Image
	coords image.Point
	coins  [NB_COINS]vCoin
}

type vCoin struct {
	imgs   [6]*ebiten.Image
	picked bool
	coords image.Point
	pos    image.Point
}

type vImg struct {
	img    *ebiten.Image
	name   string
	file   []byte
	op     *ebiten.DrawImageOptions
	center image.Point
	Size   image.Point
	coords image.Point
}

var (
	screenWidth  = 500
	screenHeight = 250
)

const (
	seaLevel     = 115
	NB_COINS int = 5 + 1
	BORDERS      = 100
)

var (
	swimmer    vImg
	boat       vImg
	background *ebiten.Image
	sky        vImg
	sea        *ebiten.Image
	blueCoast  *ebiten.Image
	coins      vImg
	coin       vCoin
)

func updateCoords(g *Game, translate float64, crement int, axis int, curr float64) {
	if axis == 1 {
		g.currentCharacter.coords.X += crement
		g.currentLevel.X += crement
		g.currentCharacter.op.GeoM.Reset()
		g.currentCharacter.op.GeoM.Translate(translate, curr)
	} else {
		g.currentCharacter.coords.Y += crement
		g.currentLevel.Y += crement
		g.currentCharacter.op.GeoM.Reset()
		g.currentCharacter.op.GeoM.Translate(curr, translate)

	}
}

func checkScreenBorders(g *Game) {
	currX := g.currentCharacter.op.GeoM.Element(0, 2)
	currY := g.currentCharacter.op.GeoM.Element(1, 2)

	if currX < -25 { // gauche
		updateCoords(g, float64(screenWidth)-6, -1, 1, currY)
	} else if int(currX) > screenWidth-5 { // droite
		updateCoords(g, 3, 1, 1, currY)
	} else if currY < 5 { // haut
		updateCoords(g, float64(screenHeight)-6, -1, 2, currX)
	} else if int(currY) > screenHeight-5 { // bas
		updateCoords(g, 6, 1, 2, currX)
	}
}

// Update proceeds the game state.
// Update is called every tick (1/60 [s] by default).
func (g *Game) Update(screen *ebiten.Image) error {
	// Write your game's logical update.

	if g.currentLevel.Y > 50 {
		//	draw water
	}
	checkScreenBorders(g)

	if ebiten.IsKeyPressed(ebiten.KeyRight) {
		g.currentCharacter.op.GeoM.Translate(3, 0)
	}
	if ebiten.IsKeyPressed(ebiten.KeyLeft) {
		g.currentCharacter.op.GeoM.Translate(-3, 0)
	}
	if g.currentCharacter == &swimmer {
		if ebiten.IsKeyPressed(ebiten.KeyDown) {
			g.currentCharacter.op.GeoM.Translate(0, 1.5)
		}
		if ebiten.IsKeyPressed(ebiten.KeyUp) && (swimmer.op.GeoM.Element(1, 2) > seaLevel || g.currentLevel.Y > 50) {
			swimmer.op.GeoM.Translate(0, -1.5)
		}
	}
	if inpututil.IsKeyJustPressed(ebiten.KeyE) {
		if g.currentCharacter == &boat {
			swimmer.op.GeoM.Reset()
			swimmer.op.GeoM.Translate(boat.op.GeoM.Element(0, 2)+20, boat.op.GeoM.Element(1, 2)+50)
			g.currentCharacter = &swimmer
		} else if g.currentCharacter == &swimmer {
			if Colision(swimmer, boat, 42, 32) == true {
				g.currentCharacter = &boat
			}
		}
	}
	currLevelCoins := g.worldMap[g.currentLevel.X][g.currentLevel.Y].coins
	if g.currentCharacter == &swimmer {
		for i := 0; i < NB_COINS; i++ {
			if LightColision(swimmer, currLevelCoins[i].pos, 10, 10) == true {
				log.Printf("you get this")
				currLevelCoins[i].picked = true
				g.worldMap[g.currentLevel.X][g.currentLevel.Y].coins[i] = currLevelCoins[i]
			}
		}
	}
	if inpututil.IsKeyJustPressed(ebiten.KeySpace) {
		printPosition(swimmer, "swimmer")
	}
	if inpututil.IsKeyJustPressed(ebiten.KeyA) {
		printPosition2(currLevelCoins)
	}
	return nil
}

// Layout takes the outside size (e.g., the window size) and returns the (logical) screen size.
// If you don't have to adjust the screen size with the outside size, just return a fixed size.
func (g *Game) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	return 500, 250
}

// Draw draws the game screen.
// Draw is called every frame (typically 1/60[s] for 60Hz display).
func (g *Game) Draw(screen *ebiten.Image) {
	// Write your game's rendering
	op := &ebiten.DrawImageOptions{}

	screen.DrawImage(background, op)
	if g.currentLevel.Y <= 50 {
		screen.DrawImage(sky.img, sky.op)
		op.GeoM.Reset()
		op.GeoM.Translate(0, 120)
		screen.DrawImage(sea, op)
		if g.currentLevel == boat.coords {
			screen.DrawImage(boat.img, boat.op)
		}
	} else if g.currentLevel.Y > 50 {
		op.GeoM.Reset()
		screen.DrawImage(sea, op)
		op.GeoM.Translate(0, 120)
		screen.DrawImage(sea, op)
		op.GeoM.Reset()
		// popping coins
		currLevelCoins := g.worldMap[g.currentLevel.X][g.currentLevel.Y].coins
		for i := 0; i < NB_COINS; i++ {
			if currLevelCoins[i].picked == true {
				continue
			}
			op.GeoM.Translate(float64(currLevelCoins[i].pos.X), float64(currLevelCoins[i].pos.Y))
			screen.DrawImage(currLevelCoins[i].imgs[0], op)
			op.GeoM.Reset()

		}
	}
	op.GeoM.Reset()

	if g.currentCharacter == &swimmer {
		screen.DrawImage(swimmer.img, swimmer.op)
	}
	msg := fmt.Sprintf(`TPS: %0.2f
Current level: %d;%d`, ebiten.CurrentTPS(), g.currentLevel.X, g.currentLevel.Y)
	ebitenutil.DebugPrint(screen, msg)
}

func init() {
	var err error
	background, err = ebiten.NewImage(screenWidth, screenHeight, 0)
	if err != nil {
		log.Fatal(err)
	}
	sky.op = &ebiten.DrawImageOptions{}
	sky.op.GeoM.Scale(2, 1)

	background.Fill(color.Gray16{0xffff})
	boat.op = &ebiten.DrawImageOptions{}
	boat.coords.X = 50
	boat.coords.Y = 50

	swimmer.op = &ebiten.DrawImageOptions{}
	swimmer.coords.X = 50
	swimmer.coords.Y = 50

}

func printPosition(toPrint vImg, name string) {
	log.Println("X", name, toPrint.op.GeoM.Element(0, 2))
	log.Println("Y", name, toPrint.op.GeoM.Element(1, 2))
}

func printPosition2(toPrint [6]vCoin) {
	for i := 0; i < NB_COINS; i++ {
		if toPrint[i].picked == false {
			log.Println("X", toPrint[i].pos, "Y")
		}
	}
}

func setLevel(game *Game) {
	// split initial png in 6 images
	var coinsImages [6]*ebiten.Image
	var r image.Rectangle
	r.Min.Y = 6
	r.Max.Y = 31
	for i := 0; i < 6; i++ {
		r.Min.X = 23 + 26*i
		r.Max.X = 26*(i+1) + 23
		coinsImages[i] = coins.img.SubImage(r).(*ebiten.Image)
	}
	// put coins in all levels
	for j := 0; j < BORDERS; j++ {
		for i := 0; i < BORDERS; i++ {
			for coin := 0; coin < NB_COINS; coin++ {
				game.worldMap[j][i].coins[coin].imgs = coinsImages
				var randCoords image.Point
				randCoords.X = rand.Intn(screenWidth-18) + 15
				randCoords.Y = rand.Intn(screenHeight-18) + 15
				game.worldMap[j][i].coins[coin].pos = randCoords
			}
		}
	}
}

func main() {
	//if runtime.GOARCH == "js" || runtime.GOOS == "js" {
	//    ebiten.SetFullScreen(true)
	//}
	game := &Game{}
	game.currentLevel.X = 50
	game.currentLevel.Y = 50

	ebiten.SetWindowSize(screenWidth, screenHeight)
	ebiten.SetWindowTitle("Exploria 404")
	blueCoast = getImage(images.BlueCoast_png)
	initVImage(&sky, images.Sky_png)
	sky.op.GeoM.Scale(2, 1)

	initVImage(&boat, images.SmallBoat_png)
	boat.op.GeoM.Translate(0, 80)
	game.currentCharacter = &boat

	initVImage(&swimmer, images.Swimmer_png)
	swimmer.op.GeoM.Translate(0, 10)

	var r image.Rectangle
	r.Min.X = 0
	r.Max.X = 900
	r.Min.Y = 390
	r.Max.Y = 700
	sea = blueCoast.SubImage(r).(*ebiten.Image)

	initVImage(&coins, images.Coins_png)

	setLevel(game)

	// Call ebiten.RunGame to start your game loop.
	if err := ebiten.RunGame(game); err != nil {
		log.Fatal(err)
	}
}
