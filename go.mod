module server.go

go 1.15

require (
	github.com/NYTimes/gziphandler v1.1.1 // indirect
	github.com/go-bindata/go-bindata v3.1.2+incompatible // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20201108214237-06ea97f0c265 // indirect
	github.com/hajimehoshi/ebiten v1.12.6
	github.com/hajimehoshi/ebiten/v2 v2.0.3
	github.com/hajimehoshi/file2byteslice v0.0.0-20200812174855-0e5e8a80490e
	github.com/oleiade/reflections v1.0.1
	gitlab.com/aboitier/exploria v0.0.0-20210117134841-f3c38386ffa8
	golang.org/x/exp v0.0.0-20201229011636-eab1b5eb1a03 // indirect
	golang.org/x/image v0.0.0-20201208152932-35266b937fa6 // indirect
	golang.org/x/sys v0.0.0-20210113181707-4bcb84eeeb78 // indirect
)
